#!/bin/bash

NC='\033[0m' # No Color
RED='\033[0;31m'
LIGHTGREEN='\033[1;32m'
CYAN='\033[0;36m'
LIGHTYELLOW='\033[1;33m'
bold=$(tput bold)
normal=$(tput sgr0)

username='bcuser'
chainname='bankchain'
homedir=`su -l $username -c 'cd ~ && pwd'`
datadir='/datadrive/.multichain'

echo '----------------------------------------'
echo -e ${CYAN}${bold}'RESTORING.....'${normal}${NC}
echo '----------------------------------------'

su -l $username -c 'multichain-cli '$chainname' -datadir='$datadir' stop'
sleep 3
rm -rf $datadir/bankchain


echo '----------------------------------------'
echo -e ${CYAN}${bold}'COMPLETED!'${normal}${NC}
echo '----------------------------------------'
