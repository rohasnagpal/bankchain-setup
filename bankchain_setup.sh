#!/bin/bash

sudo apt-get -y install pwgen gpw

chainname=bankchain
rpcuser=`gpw 1 10`
rpcpassword=`pwgen 20 1`
protocol=10008		# Not used currently in this script
networkport=61172
rpcport=15590
adminNodeName=$chainname'_Admin'
username='bcuser'
homedir=`su -l $username -c 'cd ~ && pwd'`
datadir='/datadrive/.multichain'

echo '----------------------------------------'
echo -e 'INSTALLING PREREQUISITES.....'
echo '----------------------------------------'

cd .. 

sudo apt-get --assume-yes update
sudo apt-get --assume-yes install jq git aptitude 

echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''

sleep 3
echo '----------------------------------------'
echo -e 'CONFIGURING FIREWALL.....'
echo '----------------------------------------'

sudo ufw allow $networkport
sudo ufw allow $rpcport

echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''

echo -e '----------------------------------------'
echo -e 'FIREWALL SUCCESSFULLY CONFIGURED!'
echo -e '----------------------------------------'

echo '----------------------------------------'
echo -e 'INSTALLING & CONFIGURING MULTICHAIN.....'
echo '----------------------------------------'

wget --no-verbose http://www.multichain.com/download/multichain-1.0-beta-2.tar.gz
sudo bash -c 'tar xvf multichain-1.0-beta-2.tar.gz'
sudo bash -c 'cp multichain-1.0-*/multichain* /usr/local/bin/'

su -l $username -c  "multichain-util create $chainname -datadir=$datadir"

su -l $username -c "sed -ie 's/.*root-stream-open =.*\#/root-stream-open = false     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*admin-consensus-admin =.*\#/admin-consensus-admin = 0.0     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*admin-consensus-activate =.*\#/admin-consensus-activate = 0.0     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*admin-consensus-mine =.*\#/admin-consensus-mine = 0.0     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*mining-requires-peers =.*\#/mining-requires-peers = true     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*initial-block-reward =.*\#/initial-block-reward = 0     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*first-block-reward =.*\#/first-block-reward = -1     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*target-adjust-freq =.*\#/target-adjust-freq = 172800     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*max-std-tx-size =.*\#/max-std-tx-size = 100000000     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*max-std-op-returns-count =.*\#/max-std-op-returns-count = 1024     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*max-std-op-return-size =.*\#/max-std-op-return-size = 8388608     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*max-std-op-drops-count =.*\#/max-std-op-drops-count = 100     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*max-std-element-size =.*\#/max-std-element-size = 32768     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*default-network-port =.*\#/default-network-port = '$networkport'     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*default-rpc-port =.*\#/default-rpc-port = '$rpcport'     #/g' $datadir/$chainname/params.dat"
su -l $username -c "sed -ie 's/.*chain-name =.*\#/chain-name = '$chainname'     #/g' $datadir/$chainname/params.dat"
su -l $username -c " sed -ie 's/.*protocol-version =.*\#/protocol-version = '$protocol'     #/g' $datadir/$chainname/params.dat"

su -l $username -c "echo rpcuser='$rpcuser' > $datadir/$chainname/multichain.conf"
su -l $username -c "echo rpcpassword='$rpcpassword' >> $datadir/$chainname/multichain.conf"
su -l $username -c "echo rpcport='$rpcport' >> $datadir/$chainname/multichain.conf"

echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''

echo '----------------------------------------'
echo -e 'RUNNING BLOCKCHAIN.....'
echo '----------------------------------------'

su -l $username -c 'multichaind '$chainname' -daemon -autosubscribe=assets,streams -datadir='$datadir

echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''

echo '----------------------------------------'
echo -e 'LOADING CONFIGURATION.....'
echo '----------------------------------------'

sleep 6

addr=`curl --user $rpcuser:$rpcpassword --data-binary '{"jsonrpc": "1.0", "id":"curltest", "method": "getaddresses", "params": [] }' -H 'content-type: text/json;' http://127.0.0.1:$rpcport | jq -r '.result[0]'`


echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''


echo '----------------------------------------'
echo -e 'CREATING AND CONFIGURING STREAMS.....'
echo '----------------------------------------'


# CREATE STREAMS
# ------ -------
su -l $username -c  "multichain-cli "$chainname" -datadir="$datadir" createrawsendfrom "$addr" '{}' '[{\"create\":\"stream\",\"name\":\"data\",\"open\":false,\"details\":{\"purpose\":\"Stores data related to the bankchain applications\"}}]' send"

# SUBSCRIBE STREAMS
# --------- -------
su -l $username -c  "multichain-cli "$chainname" -datadir="$datadir" subscribe data"

echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''

echo -e '----------------------------------------'
echo -e 'BLOCKCHAIN SUCCESSFULLY SET UP!'
echo -e '----------------------------------------'
echo ''
echo ''
echo ''
echo ''


echo -e 'RPC User Name - '$rpcuser
echo -e 'RPC Password - '$rpcpassword
echo -e 