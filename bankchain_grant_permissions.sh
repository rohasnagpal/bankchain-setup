#!/bin/bash

addr=$1
username='bcuser'
chainname='bankchain'
datadir='/datadrive/.multichain'

# Grant permissions
echo '----------------------------------------'
echo -e 'GRANTING PERMISSIONS:'
echo '----------------------------------------'

su -l $username -c 'multichain-cli '$chainname' -datadir='$datadir' grant '$addr' connect,send,receive,mine'

echo ''
echo ''
echo -e '----------PERMISSIONS GRANTED----------'
echo ''
echo ''