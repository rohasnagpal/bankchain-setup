#!/bin/bash
export TERM=xterm-color

NC='\033[0m' # No Color
RED='\033[0;31m'
LIGHTGREEN='\033[1;32m'
CYAN='\033[0;36m'
LIGHTYELLOW='\033[1;33m'
bold=$(tput bold)
normal=$(tput sgr0)

sudo apt-get install pwgen gpw

if [ -z "$1" ]
	then
		source /root/.digitalocean_password
		mysql_root_pass=$root_mysql_pass
	else
		mysql_root_pass=$1
fi

userName=`gpw 1 10`
newPass=`pwgen 20 1`

mysql -u root -p$mysql_root_pass -e "CREATE USER '"$userName"'@'%' IDENTIFIED BY '"$newPass"'; GRANT ALL PRIVILEGES ON aml.* TO '"$userName"'@'%';"

echo -e ${normal}${CYAN}'User Name - '${bold}${LIGHTYELLOW}$userName
echo -e ${normal}${CYAN}'Password - '${bold}${LIGHTYELLOW}$newPass

echo -e ${normal}${NC}

sudo sed -ie 's/bind-address/#bind-address/g' /etc/mysql/*.cnf /etc/mysql/*/*.cnf
sudo sed -ie 's/#.*bind-address/#bind-address/g' /etc/mysql/*.cnf /etc/mysql/*/*.cnf