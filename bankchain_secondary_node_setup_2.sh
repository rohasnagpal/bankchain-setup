#!/bin/bash

username='bcuser'
chainname='bankchain'
explorerport=2750
adminNodeName=$chainname'_local'
explorerDisplayName=$chainname
homedir=`su -l $username -c 'cd ~ && pwd'`
datadir=$homedir'/.multichain'

if [ ! -d "$datadir" ]; then
  mkdir $datadir
fi

su -l $username -c 'multichaind '$chainname' -daemon -autosubscribe=assets,streams -datadir='$datadir

source $datadir/$chainname/multichain.conf

echo '----------------------------------------'
echo -e 'LOADING CONFIGURATION.....'
echo '----------------------------------------'

sleep 6

addr=`curl --user $rpcuser:$rpcpassword --data-binary '{"jsonrpc": "1.0", "id":"curltest", "method": "getaddresses", "params": [] }' -H 'content-type: text/json;' http://127.0.0.1:$rpcport | jq -r '.result[0]'`


echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''


# SUBSCRIBE STREAMS
# --------- -------
su -l $username -c  "multichain-cli "$chainname" -datadir="$datadir" subscribe data"

echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''

echo -e '----------------------------------------'
echo -e 'BLOCKCHAIN SUCCESSFULLY SET UP!'
echo -e '----------------------------------------'
echo ''
echo ''
echo ''
echo ''

echo -e 'RPC User Name - '$rpcuser
echo -e 'RPC Password - '$rpcpassword
echo -e 