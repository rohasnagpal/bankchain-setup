#!/bin/bash

sudo apt-get -y install pwgen gpw

chainname=bankchain
rpcuser=`gpw 1 10`
rpcpassword=`pwgen 20 1`
seed_ip=$1
networkport=61172
rpcport=15590
username='bcuser'
homedir=`su -l $username -c 'cd ~ && pwd'`
datadir=$homedir'/.multichain'

echo '----------------------------------------'
echo -e 'INSTALLING PREREQUISITES.....'
echo '----------------------------------------'

cd .. 

sudo apt-get --assume-yes update
sudo apt-get --assume-yes install jq git aptitude 

echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''

sleep 3
echo '----------------------------------------'
echo -e 'CONFIGURING FIREWALL.....'
echo '----------------------------------------'

sudo ufw allow $networkport
sudo ufw allow $rpcport

echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''

echo -e '----------------------------------------'
echo -e 'FIREWALL SUCCESSFULLY CONFIGURED!'
echo -e '----------------------------------------'
echo ''
echo ''

echo '----------------------------------------'
echo -e 'INSTALLING & CONFIGURING MULTICHAIN.....'
echo '----------------------------------------'

wget --no-verbose http://www.multichain.com/download/multichain-1.0-beta-2.tar.gz
sudo bash -c 'tar xvf multichain-1.0-beta-2.tar.gz'
sudo bash -c 'cp multichain-1.0-*/multichain* /usr/local/bin/'
echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''

echo '----------------------------------------'
echo -e 'RUNNING BLOCKCHAIN.....'
echo '----------------------------------------'


if [ ! -d "$datadir" ]; then
  mkdir $datadir
fi

sudo bash -c 'chown -R '$username' '$datadir

su -l $username -c 'multichaind '$chainname'@'$seed_ip':'$networkport' -daemon -autosubscribe=assets,streams -datadir='$datadir

su -l $username -c "echo rpcuser='$rpcuser' > $datadir/$chainname/multichain.conf"
su -l $username -c "echo rpcpassword='$rpcpassword' >> $datadir/$chainname/multichain.conf"
su -l $username -c "echo rpcport='$rpcport' >> $datadir/$chainname/multichain.conf"


echo ''
echo ''
echo '----------------------------------------'
echo ''
echo ''
echo ''
echo ''
